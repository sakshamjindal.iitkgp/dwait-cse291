# %%
try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    import site
    site.main() # run this so local pip installs are recognized

# %%
# Import required packages
import gym
import gym.spaces as spaces
from tqdm.notebook import tqdm
import numpy as np
import mani_skill2.envs
import matplotlib.pyplot as plt
import torch.nn as nn
import torch as th

# %%
# the following wrappers are importable from mani_skill2.utils.wrappers.sb3
# Defines a continuous, infinite horizon, task where done is always False
# unless a timelimit is reached.
class ContinuousTaskWrapper(gym.Wrapper):
    def __init__(self, env, max_episode_steps: int) -> None:
        super().__init__(env)
        self._elapsed_steps = 0
        self._max_episode_steps = max_episode_steps

    def reset(self):
        self._elapsed_steps = 0
        return super().reset()

    def step(self, action):
        ob, rew, done, info = super().step(action)
        self._elapsed_steps += 1
        if self._elapsed_steps >= self._max_episode_steps:
            done = True
            info["TimeLimit.truncated"] = True
        else:
            done = False
            info["TimeLimit.truncated"] = False
        return ob, rew, done, info

# A simple wrapper that adds a is_success key which SB3 tracks
class SuccessInfoWrapper(gym.Wrapper):
    def step(self, action):
        ob, rew, done, info = super().step(action)
        info["is_success"] = info["success"]
        return ob, rew, done, info

# %%
from stable_baselines3.common.vec_env import SubprocVecEnv, VecMonitor
from mani_skill2.utils.wrappers import RecordEpisode
from stable_baselines3.common.utils import set_random_seed

# %%
num_envs = 8 # you can increases this and decrease the n_steps parameter if you have more cores to speed up training
env_id = "LiftCube-v1"
obs_mode = "state"
control_mode = "pd_ee_delta_pose"
reward_mode = "dense"

# define an SB3 style make_env function for evaluation
def make_env(env_id: str, max_episode_steps: int = None, record_dir: str = None):
    def _init() -> gym.Env:
        # NOTE: Import envs here so that they are registered with gym in subprocesses
        import mani_skill2.envs
        env = gym.make(env_id, obs_mode=obs_mode, reward_mode=reward_mode, control_mode=control_mode,)
        # For training, we regard the task as a continuous task with infinite horizon.
        # you can use the ContinuousTaskWrapper here for that
        if max_episode_steps is not None:
            env = ContinuousTaskWrapper(env, max_episode_steps)
        if record_dir is not None:
            env = SuccessInfoWrapper(env)
            env = RecordEpisode(
                env, record_dir, info_on_video=True, render_mode="cameras"
            )
        return env
    return _init

# create one eval environment
eval_env = SubprocVecEnv([make_env(env_id, record_dir="logs/videos") for i in range(1)])
eval_env = VecMonitor(eval_env) # attach this so SB3 can log reward metrics
eval_env.seed(0)
eval_env.reset()

# create num_envs training environments
# we also specify max_episode_steps=100 to speed up training
env = SubprocVecEnv([make_env(env_id, max_episode_steps=100) for i in range(num_envs)])
env = VecMonitor(env)
env.seed(0)
obs = env.reset()

# %%
from stable_baselines3.common.callbacks import EvalCallback, CheckpointCallback

# %%
# SB3 uses callback functions to create evaluation and checkpoints

# Evaluation: periodically evaluate the agent without noise and save results to the logs folder
eval_callback = EvalCallback(eval_env, best_model_save_path="./logs/",
                         log_path="./logs/", eval_freq=32000,
                         deterministic=True, render=False) 

checkpoint_callback = CheckpointCallback(
    save_freq=32000,
    save_path="./logs/",
    name_prefix="rl_model",
    save_replay_buffer=True,
    save_vecnormalize=True,
)

# %%
from stable_baselines3 import PPO

# %%
set_random_seed(0) # set SB3's global seed to 0
rollout_steps = 3200

# create our model
policy_kwargs = dict(net_arch=[256, 256])
model = PPO("MlpPolicy", env, policy_kwargs=policy_kwargs, verbose=1,
    n_steps=rollout_steps // num_envs, batch_size=200,
    n_epochs=15,
    tensorboard_log="./logs",
    gamma=0.85,
    target_kl=0.05
)

# %%
# %load_ext tensorboard

# %%
# Train with PPO
model.learn(1_000_000, callback=[checkpoint_callback, eval_callback])
model.save("./logs/latest_model")

# optionally load back the model that was saved
model = model.load("./logs/latest_model")

# %%
# # Code for simply loading a pretrained policy
# import gdown
# gdown.download("https://drive.google.com/uc?id=1KTBjPW3SN-mrRpkESvC_GE8QLBtvdVAS&export=download", output="./logs/pretrained_model.zip")
# model.policy = model.load("logs/pretrained_model").policy

# %%
from stable_baselines3.common.evaluation import evaluate_policy

# %%
eval_env.close() # close the old eval env
# make a new one that saves to a different directory
eval_env = SubprocVecEnv([make_env(env_id, record_dir="logs/eval_videos") for i in range(1)])
eval_env = VecMonitor(eval_env) # attach this so SB3 can log reward metrics
eval_env.seed(1)
eval_env.reset()

returns, ep_lens = evaluate_policy(model, eval_env, deterministic=True, render=False, return_episode_rewards=True, n_eval_episodes=10)
success = np.array(ep_lens) < 200 # episode length < 200 means we solved the task before time ran out
success_rate = success.mean()
print(f"Success Rate: {success_rate}")
print(f"Episode Lengths: {ep_lens}")

# # %%
# from IPython.display import Video
# Video("./logs/eval_videos/2.mp4", embed=True) # Watch one of the replays

# %%



